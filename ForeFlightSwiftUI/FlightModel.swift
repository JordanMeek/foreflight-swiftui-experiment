//
//  FlightModel.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 12/3/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import Foundation

enum AltitudeReferenceDatum: String {
    case MSL = "MSL"
    case AGL = "AGL"
}

struct flight: Identifiable {
    var id = UUID()
    var departureAirportICAOcode: String
    var destinationAirportICAOcode: String
    var altitudeFeetMSL: Int
    var altitudeReferenceDatum: AltitudeReferenceDatum
}
