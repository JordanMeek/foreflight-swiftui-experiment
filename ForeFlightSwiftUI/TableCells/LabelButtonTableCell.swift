//
//  LabelButtonTableCell.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 9/30/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

struct LabelButtonTableCell: View {
    var labelText: String
    var buttonText: String
    var cellButtonText: String
    
    @State var cellButtonIsVisible: Bool = false
    
    var body: some View {
        HStack {
            Text(self.labelText)
            Spacer()
            if cellButtonIsVisible {
                TableCellButton(cellButtonText: cellButtonText)
            }
            Button(action: {
                // your action here
            }) {
                Text(buttonText)
            }
        }
    }
}

struct LabelButtonTableCell_Previews: PreviewProvider {
    static var previews: some View {
        LabelButtonTableCell(labelText: "Label", buttonText: "Button", cellButtonText: "Cell Button", cellButtonIsVisible: true)
    }
}
