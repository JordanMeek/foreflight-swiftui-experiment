//
//  SingleTableCellButton.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 10/3/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

struct SingleTableCellButton: View {
    var buttonText: String
    
    var body: some View {
        HStack(alignment: .center) {
            Spacer()
            Button(action: {
                print("Button Tapped")
            }) {
                Text(buttonText)
                    .fontWeight(.semibold)
                    .foregroundColor(Color.green)
            }
            Spacer()
        }
    }
}

struct SingleTableCellButton_Previews: PreviewProvider {
    static var previews: some View {
        SingleTableCellButton(buttonText: "Test123")
    }
}
