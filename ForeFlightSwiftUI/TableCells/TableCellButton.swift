//
//  TableCellButton.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 10/9/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

struct TableCellButton: View {
    var cellButtonText: String
    
    var body: some View {
        Button(action: {
            // your action here
        }) {
            Text(cellButtonText)
                .font(.system(size: 10))
                .fontWeight(.medium)
                .frame(width: 65, height: 25, alignment: .center)
                .overlay(
                    RoundedRectangle(cornerRadius: 2)
                        .stroke(Color("activeSelectedCTA"), lineWidth: 0.5)
                )
        }
    }
}

struct TableCellButton_Previews: PreviewProvider {
    static var previews: some View {
        TableCellButton(cellButtonText: "Test123")
    }
}
