//
//  FlightSummaryCell.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 11/15/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

struct FlightSummaryCell: View {
    var body: some View {
        HStack(alignment: .firstTextBaseline) {
            VStack(alignment: .leading) {
                Text("KJFK to KLAX").font(.system(size: 14))
                Text("17,000' MSL in N123AB").font(.system(size: 14))
                Text("RBV J230 BYRDD J48 EMI J211 JERES J220 AML J213 KERRE MILLK SACKO GLASS YUGTU STACS EPAYI POAKE BUTTE MUMTE GABBL HLYWD1").font(.system(size: 14))
            }
            Spacer()
            VStack(alignment: .trailing) {
                Text("Nov 20th, 2019")
                    .foregroundColor(Color("activeSelectedCTA"))
                    .font(.system(size: 14))
            }
        }
    }
}

struct FlightSummaryCell_Previews: PreviewProvider {
    static var previews: some View {
        FlightSummaryCell()
    }
}
