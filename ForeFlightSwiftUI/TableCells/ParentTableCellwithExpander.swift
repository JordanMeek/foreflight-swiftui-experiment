//
//  ParentTableCellwithExpander.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 10/9/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

struct ParentTableCellwithExpander: View {
    var labelText: String
    var buttonText: String
    
    @State private var showDetail = true
    
    var body: some View {
        HStack {
            Button(action: {
                self.showDetail.toggle()
            }) {
                Image("expand-arrow")
                    .rotationEffect(.degrees(showDetail ? -90 : 0))
                    //.scaleEffect(showDetail ? 1.5 : 1)
                    //.padding()
                    //.animation(.easeInOut())
                    .foregroundColor(.black)
                    //.scaleEffect(0.7)
            }
            Text(self.labelText)
            Spacer()
            Button(action: {
                // your action here
            }) {
                Text(buttonText)
            }
        }
    }
}

struct ParentTableCellwithExpander_Previews: PreviewProvider {
    static var previews: some View {
        ParentTableCellwithExpander(labelText: "Label", buttonText: "Button")
    }
}
