//
//  LongInputTableCell.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 10/1/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

struct LongInputTableCell: View {
    @State private var route: String = ""
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text("Route")
            TextField("DIRECT", text: $route)
        }
    }
}

struct LongInputTableCell_Previews: PreviewProvider {
    static var previews: some View {
        LongInputTableCell()
    }
}
