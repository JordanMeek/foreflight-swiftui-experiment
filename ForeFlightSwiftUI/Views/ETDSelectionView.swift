//
//  ETDSelectionView.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 10/3/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

struct ETDSelectionView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello World!"/*@END_MENU_TOKEN@*/)
    }
}

struct ETDSelectionView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ETDSelectionView()
                .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
                .previewDisplayName("iPhone SE")
            ETDSelectionView()
                .previewDevice(PreviewDevice(rawValue: "iPhone X"))
                .previewDisplayName("iPhone X")
            ETDSelectionView()
                .previewDevice(PreviewDevice(rawValue: "iPad Pro (10.5-inch)"))
                .previewDisplayName("iPad Pro (10.5-inch)")
        }
    }
}
