//
//  AllFlightsListView.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 10/3/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

struct AllFlightsListView: View {
    var body: some View {
//        VStack(alignment: .leading) {
//            ForEach((1...10).reversed(), id: \.self) {
//                Text("\($0)…")
//            }
//
//            Text("Ready or not, here I come!")
//        }
        List(0..<5){item in
            NavigationLink(destination: Flights()) {
                FlightSummaryCell().navigationBarTitle("Flights", displayMode: .inline)
            }
        }
    }
}

struct AllFlightsListView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            AllFlightsListView()
                .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
                .previewDisplayName("iPhone SE")
            AllFlightsListView()
                .previewDevice(PreviewDevice(rawValue: "iPhone X"))
                .previewDisplayName("iPhone X")
            AllFlightsListView()
                .previewDevice(PreviewDevice(rawValue: "iPad Pro (10.5-inch)"))
                .previewDisplayName("iPad Pro (10.5-inch)")
        }
    }
}
