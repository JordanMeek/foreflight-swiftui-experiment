//
//  PerformanceProfileView.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 10/1/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

struct PerformanceProfileView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello World!"/*@END_MENU_TOKEN@*/)
    }
}

struct PerformanceProfileView_Previews: PreviewProvider {
    static var previews: some View {
        PerformanceProfileView()
    }
}
