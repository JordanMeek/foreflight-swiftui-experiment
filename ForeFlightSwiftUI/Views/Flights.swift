//
//  Flights.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 9/20/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

struct Flights: View {
    var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, yyyy hh:mm zzz"
        return formatter
    }
    
    var body: some View {
        VStack(spacing:0) {
            ScrollViewHeader()
            Form {
                Section(header: Text("DEPARTURE/DESTINATION")) {
                    LabelButtonTableCell(labelText: "Departure", buttonText: "Required", cellButtonText: "INFO", cellButtonIsVisible: true)
                    LabelButtonTableCell(labelText: "Destination", buttonText: "Required", cellButtonText: "INFO", cellButtonIsVisible: true)
                    LabelButtonTableCell(labelText: "Alternate", buttonText: "", cellButtonText: "")
                    NavigationLink(destination: ETDSelectionView().navigationBarTitle("ETD")) {
                        LabelButtonTableCell(labelText: "ETD", buttonText: dateFormatter.string(from: Date.init()), cellButtonText: "").navigationBarTitle("KBWI to KEWR", displayMode: .inline)
                    }
                }
                Section(header: Text("AIRCRAFT")) {
                    NavigationLink(destination: AircraftSelectionView().navigationBarTitle("Aircraft")) {
                        LabelButtonTableCell(labelText: "Aircraft", buttonText: "C172", cellButtonText: "")
                    }
                    NavigationLink(destination: PerformanceProfileView().navigationBarTitle("Performance Profile")) {
                        LabelButtonTableCell(labelText: "Performance Profile", buttonText: "2500 RPM", cellButtonText: "")
                    }
                }
                Section(header: Text("ROUTE")) {
                    NavigationLink(destination: FlightRulesView().navigationBarTitle("Flight Rules")) {
                        LabelButtonTableCell(labelText: "Flight Rules", buttonText: "IFR", cellButtonText: "")
                    }
                    MapView()
                        .frame(height:200)
                    LongInputTableCell()
                    SingleTableCellButton(buttonText: "Routes (9)")
                    Text("Cruise Altitude")
                }
                Section(header: Text("PAYLOAD (LBS)")) {
                    LabelButtonTableCell(labelText: "People", buttonText: "200", cellButtonText: "")
                    LabelButtonTableCell(labelText: "Cargo", buttonText: "0", cellButtonText: "")
                    LabelButtonTableCell(labelText: "Total Payload", buttonText: "200", cellButtonText: "")
                }
                Section(header: Text("Fuel")) {
                    NavigationLink(destination: FuelPolicyView().navigationBarTitle("Fuel Policy")) {
                        LabelButtonTableCell(labelText: "Fuel Policy", buttonText: "Minimum Fuel Required", cellButtonText: "")
                    }
                    ParentTableCellwithExpander(labelText: "Text", buttonText: "Text1")
                }
                
            }
            .navigationBarTitle("KBWI to KEWR") //.listStyle(GroupedListStyle())
            //.background(Color.clear)
            
        }.background(Color.green)
    }
}


struct Flights_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            Flights()
                .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
                .previewDisplayName("iPhone SE")
            Flights()
                .previewDevice(PreviewDevice(rawValue: "iPhone X"))
                .previewDisplayName("iPhone X")
            Flights()
                .previewDevice(PreviewDevice(rawValue: "iPad Pro (10.5-inch)"))
                .previewDisplayName("iPad Pro (10.5-inch)")
        }
    }
}
