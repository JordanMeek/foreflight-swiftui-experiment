//
//  ScrollViewHeader.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 10/3/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

struct ScrollViewHeader: View {
    var body: some View {
        HStack (spacing: 30) {
            VStack (alignment: .leading) {
                Text("Dist")
                    .font(.system(size: 12))
                    .fontWeight(.medium)
                Text("149 nm")
                    .font(.system(size: 15))
                    .fontWeight(.medium)
            }
            VStack (alignment: .leading) {
                Text("ETE")
                    .font(.system(size: 12))
                    .fontWeight(.medium)
                Text("1h19m")
                    .font(.system(size: 15))
                    .fontWeight(.medium)
            }
            VStack (alignment: .leading) {
                Text("ETA")
                    .font(.system(size: 12))
                    .fontWeight(.medium)
                Text("17:14")
                    .font(.system(size: 15))
                    .fontWeight(.medium)
            }
            VStack (alignment: .leading) {
                Text("Flight Fuel")
                    .font(.system(size: 12))
                    .fontWeight(.medium)
                Text("13.8 g")
                    .font(.system(size: 15))
                    .fontWeight(.medium)
            }
            VStack (alignment: .leading) {
                Text("Wind")
                    .font(.system(size: 12))
                    .fontWeight(.medium)
                Text("3 kts tail")
                    .font(.system(size: 15))
                    .fontWeight(.medium)
            }
        }
        .frame(minWidth: 320, maxWidth: .infinity, minHeight: 60, maxHeight: 60, alignment: .center)
        .background(Color("background"))
    }
}

struct ScrollViewHeader_Previews: PreviewProvider {
    static var previews: some View {
        ScrollViewHeader()
    }
}
