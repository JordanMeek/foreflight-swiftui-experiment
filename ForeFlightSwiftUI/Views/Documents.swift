//
//  Documents.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 9/20/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

struct Documents: View {
    var body: some View {
        Text("Documents")
    }
}

struct Documents_Previews: PreviewProvider {
    static var previews: some View {
        Documents()
    }
}
