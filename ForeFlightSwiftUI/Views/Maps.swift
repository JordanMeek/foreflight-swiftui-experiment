//
//  Maps.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 9/20/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

struct Maps: View {
    var body: some View {
        MapView()
    }
}

struct Maps_Previews: PreviewProvider {
    static var previews: some View {
        Maps()
    }
}
