//
//  ContentView.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 9/20/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var selection = 0
    
    init() {
        UITabBar.appearance().barTintColor = UIColor.black
        UITabBar.appearance().backgroundColor = UIColor.white
    }
 
    var body: some View {
        TabView(selection: $selection){
            Airports()
                .font(.title)
                .tabItem {
                    VStack {
                        Image("airports")
                        Text("Airports")
                    }
                }
                .tag(0)
            Maps()
                .font(.title)
                .tabItem {
                    VStack {
                        Image("maps")
                        Text("Maps")
                    }
                }
                .tag(1)
            Documents()
                .font(.title)
                .tabItem {
                    VStack {
                        Image("documents")
                        Text("Documents")
                    }
                }
                .tag(2)
            NavigationView {
                AllFlightsListView().navigationBarTitle("Flights", displayMode: .inline)
                Flights()
            }
            .navigationViewStyle(DoubleColumnNavigationViewStyle())
            .padding(1)
                //.font(.title)
                .tabItem {
                    VStack {
                        Image("flights")
                        Text("Flights")
                    }
                }
                .tag(3)
            Plates()
                .font(.title)
                .tabItem {
                    VStack {
                        Image("plates")
                        Text("Plates")
                    }
                }
                .tag(4)
            Imagery()
                .font(.title)
                .tabItem {
                    VStack {
                        Image("imagery")
                        Text("Imagery")
                    }
                }
                .tag(5)
            
            ScratchPads()
                .font(.title)
                .tabItem {
                    VStack {
                        Image("scratchpads")
                        Text("ScratchPads")
                    }
                }
                .tag(6)
        }
        .accentColor(Color("activeSelectedCTA"))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
                .previewDevice(PreviewDevice(rawValue: "iPhone SE"))
                .previewDisplayName("iPhone SE")
//            ContentView()
//                .previewDevice(PreviewDevice(rawValue: "iPhone X"))
//                .previewDisplayName("iPhone X")
            ContentView()
                .previewDevice(PreviewDevice(rawValue: "iPad Pro (10.5-inch)"))
                .previewDisplayName("iPad Pro (10.5-inch)")
        }
    }
}
