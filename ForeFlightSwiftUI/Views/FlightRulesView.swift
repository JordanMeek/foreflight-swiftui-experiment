//
//  FlightRulesView.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 10/1/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

struct FlightRulesView: View {
    var body: some View {
        VStack {
            Picker(selection: .constant(1), label: Text("Flight Rules")) {
                Text("IFR").tag(1)
                Text("VFR").tag(2)
                Text("Y (IFR to VFR)").tag(3)
                Text("Z (VFR to IFR)").tag(4)
                Text("VFR (DC SFRA)").tag(5)
                }
        }
    }
}

struct FlightRulesView_Previews: PreviewProvider {
    static var previews: some View {
        FlightRulesView()
    }
}
