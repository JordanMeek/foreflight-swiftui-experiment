//
//  Imagery.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 9/20/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

struct Imagery: View {
    var body: some View {
        Text("Imagery")
    }
}

struct Imagery_Previews: PreviewProvider {
    static var previews: some View {
        Imagery()
    }
}
