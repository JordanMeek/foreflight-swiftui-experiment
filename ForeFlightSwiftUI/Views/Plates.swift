//
//  Plates.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 9/20/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

struct Plates: View {
    var body: some View {
        Text("Plates")
    }
}

struct Plates_Previews: PreviewProvider {
    static var previews: some View {
        Plates()
    }
}
