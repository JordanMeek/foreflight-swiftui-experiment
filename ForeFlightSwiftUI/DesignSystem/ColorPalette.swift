//
//  ColorPalette.swift
//  ForeFlightSwiftUI
//
//  Created by Jordan Meek on 11/20/19.
//  Copyright © 2019 Jordan Meek. All rights reserved.
//

import SwiftUI

internal extension UIColor {
    class var ryanBlue: UIColor              { return #colorLiteral(red: 0.20392157137393951, green: 0.5960784554481506, blue: 0.8588235378265381, alpha: 1.0) }
}
